📦 Version and System
------------------------------------------
<!-- 
  🎓 Instructions for obtaining information about your system
  1️⃣ Open 💻 Gnome Terminal
  2️⃣ Paste these Commands into 💻 Gnome Terminal:
      gnome-shell --version
      nautilus --version
      cat /etc/*-release
      cat $HOME/.local/share/gnome-shell/extensions/desktop-icons@csoriano/metadata.json
  3️⃣ Paste the Output of the Commands within the ticks below, where [here] is written

--> 

```
[here]
```

Optionally, can it be reproduced with the git version in this repository?

📜 Description of the Issue
-------------------------------




🎥 Screencast or Screenshot
-------------------------------
<!-- 
 Go to the following link for instructions how to screencast: https://help.gnome.org/users/gnome-help/stable/screen-shot-record.html.en#screencast

Alternatively an image of the result might be helpful.
--> 

